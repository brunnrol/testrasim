# Simulating Testing strategies for virus infections

## The Idea
The idea for this project emerged while watching discussions in the news about COVID19 testing strategies. When wondering, 
if there was an efficient way to reliably predict the number of infected people based on ad-hoc testing data, I remembered 
that in population biology the [Lincoln/Peterson Method](https://en.wikipedia.org/wiki/Mark_and_recapture) is used to estimate
the size of a population. A quiet voice in the back of my head whispered, that this might be a way to make use of existing 
testing data to estimate the currently infected population. Coincidentally, it was easter and I was locked down at home. So 
some coding had to happen to prove the hypothesis, that when testing a given random selection of the population twice, one
is able to make accurate estimation on the total of infected people.

### The Lincoln/Peterson method (mark and recapture)
The Mark and Recapture method works by going into the wild, capture a number of animals of one species, mark them with some
feature, that cannot be removed. Then at a later stage you go back, capture another number of animals and check how many of them
were marked in the first iteration.
Based on the number of marked animals in the second sample, you can run some estimation on the total population size.
Is this really applicable to the problem at hand? No idea, coding first, thinking afterwards :)
 
## The Approach
In order to prove the hypothesis, I implemented a three step approach:
1. Simulate Situation
2. Simulate Testing Campaign
3. Compare Estimation of testing campaign against actual value

### Simulation
A simulation is necessary to create lab conditions of a "true and real situation". To validate our hypothesis, we need to know
the actual number of infected people. Hence, we need to propose an epidemiological model. The model in this project is 
represented in the [Simulation.py](./Simulation.py) file.
The model assumes that each member of the population is either `healthy`, `infected` or `recovered`. There is a certain chance
for healthy people to become infected and for infected people to become recovered. However, the model does not foresee any 
other transitions. This is modelled with the parameters in the table below

| Parameter               | Description  |
| -------------           |:-------------|
| Population Size         | Number of simulated people |
| Duration                | Number of simulated time step. Simulation terminates early if the number of infected people = 0 |
| Initial infection Rate  | Number of infected people at start of simulation |
| Base infection Rate     | Probability of a healthy person to become infected in a time step. This number is multiplied with the % of infected people in the population in order to calculate the actual probability in a time step |
| Recovery Rate           | Probability of a infected person to become recovered in a time step |

The simulation yields a csv file in the [data folder](./data) and csv file in the [summary folder](./summary).
The file in the data folder holds the state of each simulated person in each time step.
The time steps are rows and the people are columns. This is used in the next step to simulate the testing campaign.
The file in the summary folder hold the sum of `healthy`, `infected` and `recovered` people at each time step. This file is 
used for visualizing the situation.

### Simulation of testing campaign
The testing campaign is simulated by selecting two time steps out of a situation. We then randomly select a number of people (`sample size`) out
of the simulated population and evaluating the condition of these randomly selected people.
For now, the testing campaign is based on testing the same people twice at `sampling time 1` and a later `sampling time 2` with constant `sample size`.
Of course, other testing strategies could be implemented.
In a next step, we evaluate the number of `healthy`, `infected` and `recovered` people in our sample.
The testing is implemented in [Testing.py](Testing.py)

| Parameter         | Description  |
| -------------     |:-------------|
| File Name         | file name with simulation of actual situation |
| Sampling time 1   | index of time step where we test the first time |
| Sampling time 2   | index of time step where we test the second time |
| sample size       | number of people in our sample |


### Evaluating the testing strategy
In order to evaluate the testing strategy, we compute the estimated number of infections based on the mark and capture formula.
We then calculate the deviation in % from the actual value in order to define the accuracy of the prediction. A perfect prediction, hence,
would yield 100% accuracy.
In order to test the initial hypothesis, we benchmark the prediction against the simples of all testing strategies:
test a significant number of people once and extrapolate to the whole population.

## Disclaimer
This project rather more fun than science. It's a fun way to discover data science with numpy & pandas, visualisation
with dash and plotly, while applying it to a current situation. So far, I have done no research whatsoever on the usual 
methods to simulate epidemics or on testing strategies. Hence, any conclusion out of this project is not reliable.
However, if you feel like contributing, feel free to go ahead an give this project more real world relevance.

## Next steps
There's ton to do in order to improve this project.

### Science stuff
* Fit model parameters to real world data in order to get a relevant model
* Analyse parameter sensitivity of  model
* Select a few interesting models and evaluate the proposed testing strategy with different parameter sets
* I'm quite sure, that one could model this without simulating random events.

### New features
* allow dynamic selection of sampling time frames
* visualise result of testing strategy
* allow ad hoc simulation and plotting with given simulation parameter sets
* build a repository of simulation and make them available for plotting

### Bugfixes
* multithreading does not seem to work properly
