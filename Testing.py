import random

import numpy as np
import pandas as pd

import Simulation as Sim
import Visualisation as Vis

path = "./data/"


def load_data(fname):
    df = pd.read_csv(path + fname)
    return df


def compute_slice_actuals(data, time):
    slice = data.iloc[time, :].tolist()
    return {'time': time, 'healthy': slice.count(Sim.not_infected), 'infected': slice.count(Sim.infected),
            'recovered': slice.count(Sim.recovered), 'data': slice}


def compute_sample_actuals(data, sample_population, time):
    slice = np.asarray(data)[sample_population].tolist()
    return {'time': time, 'healthy': slice.count(Sim.not_infected), 'infected': slice.count(Sim.infected),
            'recovered': slice.count(Sim.recovered), 'data': data}


def compute_sample_population(meta_data, sample_size):
    population_size = int(meta_data['population'])
    index_range = range(0, population_size - 1)
    return random.sample(index_range, k=round(population_size * sample_size))


def compute_lincoln_peterson_estimation(sample1_marked, sample2_marked, sample_size):
    # Estimate actual population with lincoln peterson method
    return (sample1_marked * sample_size) / sample2_marked


def compute_benchmark_estimation(sample2_infected, sample_size, population_size):
    return (sample2_infected / sample_size) * population_size


def calculate_accuracy(estimated_infections, actual_infections):
    return (abs(1 - abs(((estimated_infections - actual_infections) / actual_infections)))) * 100


def compute_testing_accuracy(data, meta_data, sampling_time_1, sampling_time_2, sample_size):
    # Select sample time
    slice1 = compute_slice_actuals(data, sampling_time_1)
    slice2 = compute_slice_actuals(data, sampling_time_2)

    # Build Sample points
    sample_population = compute_sample_population(meta_data, sample_size)
    sample1 = compute_sample_actuals(slice1['data'], sample_population, sampling_time_1)
    sample2 = compute_sample_actuals(slice2['data'], sample_population, sampling_time_2)

    # Calculate Lincoln peterson index
    estimated_infections = compute_lincoln_peterson_estimation(sample1['infected'], sample2['infected'],
                                                               len(sample_population))
    benchmark_infections = compute_benchmark_estimation(sample2['infected'], len(sample_population),
                                                        int(meta_data['population']))
    accuracy_benchmark_estimation = calculate_accuracy(benchmark_infections, slice2['infected'])
    accuracy_lp_estimation = calculate_accuracy(estimated_infections, slice2['infected'])
    '''
    print("Benchmark - Estimated infections: {} - Actual infections: {} - estimation accuracy: {}".format(
        benchmark_infections,
        slice2['infected'],
        accuracy_benchmark_estimation))
    print("Lincoln Peterson Index - Estimated infections: {} - Actual infections: {} - estimation accuracy: {}".format(
        estimated_infections,
        slice2['infected'],
        accuracy_lp_estimation))
    '''
    result = {'Actual': slice2['infected'], 'Benchmark-Estimation': benchmark_infections,
              'Benchmark-Accuracy': accuracy_benchmark_estimation, 'LP-Estimation': estimated_infections,
              'LP-Accuracy': accuracy_lp_estimation}
    return result


def compute_average_performace(file_name, sampling_time_1, sampling_time_2, sample_size):
    # Load data
    data = load_data(file_name)
    meta_data = Vis.parse_parameter(file_name)
    # Run sampling
    results = []
    for i in range(100):
        results.append(compute_testing_accuracy(data, meta_data, sampling_time_1, sampling_time_2, sample_size))
    df = pd.DataFrame(data=results)
    hypothesis_confirmed = True if df['Benchmark-Accuracy'].mean() < df['LP-Accuracy'].mean() else False
    return {'Mean-Benchmark': df['Benchmark-Accuracy'].mean(), 'Std-Benchmark': df['Benchmark-Accuracy'].std(),
            'Mean-LP': df['LP-Accuracy'].mean(), 'Std-LP': df['LP-Accuracy'].std(), 'LP-Wins': hypothesis_confirmed}
