import concurrent.futures as futures
import os
from os import listdir
from os.path import isfile, join
from random import randint

import dash
import dash_core_components as dcc
import dash_html_components as html
import flask

import Simulation as Sim
import Visualisation as Figures

external_css = ["https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"]
external_js = ['https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js']

# Simulation parameters
population_size = 100_000
duration = 10_000
initial_infection_rates = [0.1, 0.3, 0.4, 0.5]  # [0.1]

# Infection Parameters
base_infection_rates = [0.1, 0.2, 0.5, 0.8, 0.9, 1]  # [0.1]
recovery_rates = [0.001, 0.01, 0.05, 0.1, 0.2,
                  0.3]  # [0.001]# # todo: calculated value for assumption after 10 => 99% recovered cases

# Setup Server
server = flask.Flask(__name__)
server.secret_key = os.environ.get('secret_key', str(randint(0, 1000000)))
app = dash.Dash(__name__, server=server, external_stylesheets=external_css, external_scripts=external_js)

# Simulation queue
queue = []

# Render Page
child = [html.H1(children='Simulation Overview', style={'textAlign': 'center'}),
         html.Div(
             children='This page is a fun project over easter to simulate an epidemic and evaluate testing strategies.'),
         html.A(href='https://gitlab.com/brunnrol/testrasim', children='Find more on Gitlab.com'),
         html.H2(children='Run new simulation'),
         html.Div(className='row', children=[
             html.Form(
                 children=[html.Div(className='row', children=[
                     html.Div(children=[
                         dcc.Input(id='iir', type='number',
                                   min=0.01, max=0.99,
                                   step=0.001),
                         html.Label(children='Initial infection Rate', htmlFor='iir')]
                         , className='input-field col s6')]),
                           html.Div(className='row',
                                    children=[html.Div(children=[dcc.Input(id='bir',
                                                                           type='number',
                                                                           min=0.01, max=0.99,
                                                                           step=0.001),
                                                                 html.Label(children='Base infection Rate',
                                                                            htmlFor='bir')
                                                                 ],
                                                       className='input-field col s6')]),
                           html.Div(className='row', children=[html.Div(children=[dcc.Input(id='rr',
                                                                                            type='number',
                                                                                            min=0.01, max=0.99,
                                                                                            step=0.001),
                                                                                  html.Label(children='Recovery Rate',
                                                                                             htmlFor='rr')
                                                                                  ],
                                                                        className='input-field col s6')]),

                           ],
                 className='col s12'),
             html.Div(className='row', children=[html.Div(
                 children=[html.Button(id='run-simulation', children='Run Simulation', className='btn')],
                 className='input-field col s6')]), ]),
         html.Div(id='simulation-output'),
         html.H2(children='Current Simulation queue'),
         html.Div(children=html.Div(id='queue2', className='collection'), className='row'),
         html.H2(children='Select your Scenarios'),
         dcc.Dropdown(id='scenarioSelection',
                      multi=True),
         html.Div(children=html.Div(id='graphs'), className='row'),
         dcc.Interval(id='graph-update', interval=1000, n_intervals=0),
         dcc.Interval(id='queue-update', interval=10000, n_intervals=0),
         dcc.Interval(id='queue-processed', interval=1000, n_intervals=0),
         dcc.Interval(id='read-files', interval=1000, n_intervals=0),
         html.Div(children=html.Div(id='queue'), className='row')
         ]
app.layout = html.Div(children=child, className='container')


def does_simulation_exist(iir, bir, rr):
    for file in listdir(Figures.path):
        if isfile(join(Figures.path, file)):
            params = Figures.parse_parameter(file)
            if params['initialInfectionRate'] == str(iir) and params['baseInfectionRate'] == str(bir) \
                    and params['recoveryRate'] == str(rr):
                return True
    return False


@app.callback(
    dash.dependencies.Output('simulation-output', 'children'),
    [dash.dependencies.Input('run-simulation', 'n_clicks')],
    [
        dash.dependencies.State('iir', 'value'),
        dash.dependencies.State('bir', 'value'),
        dash.dependencies.State('rr', 'value')],
)
def create_simulation(n_clicks, iir, bir, rr):
    if n_clicks is None:
        return html.Div(children='Nothing submitted')
    else:
        validation = []
        # Validate input
        if iir is None:
            validation.append(create_validation_message(
                "The initial infection rate is not set or not in the range between 0.01 and 0.99."))
        if bir is None:
            validation.append(create_validation_message(
                "The base infection rate is not set or not in the range between 0.01 and 0.99."))
        if rr is None:
            validation.append(
                create_validation_message("The recovery rate is not set or not in the range between 0.01 and 0.99."))
        # Validate existing simulation
        if does_simulation_exist(iir, bir, rr):
            validation.append(
                create_validation_message(
                    "A simulation with these parameters already exists."))

        if len(validation) > 0:
            return html.Div(className='collection', children=validation)
        else:
            enqueue_simulation(iir, bir, rr, population_size, duration)
            result = [create_validation_message(
                "Simulation submitted with Inital infection {} base infection {} and recovery {}".format(iir, bir, rr))]
            return html.Div(className='collection', children=result)


def enqueue_simulation(iir, bir, rr, population, dur):
    exists = False
    for sim in queue:
        if sim[0] == iir and bir == sim[1] and rr == sim[2]:
            exists = True
    if exists:
        print("Entry already enqueued")
    else:
        queue.append([iir, bir, rr, population, dur, False, False])


def process_queue():
    for sim in queue:
        if not sim[5]:
            print("Submitting simulation")
            with futures.ProcessPoolExecutor(max_workers=8) as executor:
                sim[5] = True
                executor.map(
                    Sim.run_simulation(sim[0], sim[1], sim[2], sim[3], sim[4]))


def update_done_tasks():
    result = []
    for sim in queue:
        if not sim[6]:
            if does_simulation_exist(sim[0], sim[1], sim[2]):
                sim[6] = True
            else:
                result.append(create_validation_message(
                    "Simulation with Inital infection {} base infection {} and recovery {}".format(sim[0], sim[1],
                                                                                                   sim[2])))
    return result


def create_validation_message(message):
    return html.Div(className='collection-item', children=[message])


@app.callback(dash.dependencies.Output('scenarioSelection', 'options'),
              [dash.dependencies.Input('read-files', 'n_intervals')], )
def update_files(_):
    return [{'label': Figures.build_label(Figures.parse_parameter(s)), 'value': s} for s in
            listdir(Figures.path) if
            isfile(join(Figures.path, s))]


@app.callback(
    dash.dependencies.Output('graphs', 'children'),
    [dash.dependencies.Input('scenarioSelection', 'value'),
     dash.dependencies.Input('graph-update', 'n_intervals')],
)
def update_graphs(file_names, n):
    return Figures.get_figures(file_names)


@app.callback(
    dash.dependencies.Output('queue', 'children'),
    [dash.dependencies.Input('queue-update', 'n_intervals')],
)
def update_queue(_):
    process_queue()


@app.callback(
    dash.dependencies.Output('queue2', 'children'),
    [dash.dependencies.Input('queue-processed', 'n_intervals')],
)
def update_queue_done(_):
    return update_done_tasks()


def run_simulations():
    with futures.ProcessPoolExecutor(max_workers=8) as executor:
        for initial_infection_rate in initial_infection_rates:
            print("#################################")
            print("Running Simulations for infection rate ", initial_infection_rate)
            print("#################################")
            for base_infection_rate in base_infection_rates:
                for recovery_rate in recovery_rates:
                    executor.submit(Sim.run_simulation(initial_infection_rate, base_infection_rate, recovery_rate,
                                                       population_size, duration))


if __name__ == '__main__':
    run_simulation = False
    if run_simulation:
        run_simulations()
    else:
        # Sampling parameter
        sample_time_1 = 10
        sample_time_2 = 15
        sample_size = 0.05
        fn = "data_iterations_10000_popSize_10000_initialInfectionRate_0.1_baseInfectionRate_0.1_recoveryRate_0.001_20200412_1056.gz"
        # result = Test.compute_average_performace(fn, sample_time_1, sample_time_2, sample_size)
        # print(result)
        app.run_server(debug=True, threaded=True)
