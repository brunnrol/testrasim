from datetime import datetime
from random import random

import numpy as np

# Constants
not_infected = 0
infected = 1
recovered = 2


def compute_current_infection_rate(number_current_infections, base_infection_rate, population_size):
    return base_infection_rate * (number_current_infections / population_size)


def compute_transition(current_infection_rate, current_state, recovery_rate):
    if current_state == not_infected:
        # Calculate infection
        if random() >= current_infection_rate:
            return not_infected
        else:
            return infected
    elif current_state == infected:
        # Calculate recovery
        if random() >= recovery_rate:
            return infected
        else:
            return recovered
    else:
        # Calculate reinfection - Assumption: no reinfections
        return recovered


def seed_simulation(initial_infection_rate, population_size):
    seed_array = []
    for x in range(population_size):
        if random() > initial_infection_rate:
            seed_array.append(not_infected)
        else:
            seed_array.append(infected)
    return seed_array


def compute_file_name(duration, population_size, initial_infection_rate, base_infection_rate, recovery_rate):
    now = datetime.now()
    date_time = now.strftime("%Y%m%d_%H%M")
    return "iterations_{}_popSize_{}_initialInfectionRate_{}_baseInfectionRate_{}_recoveryRate_{}_{}.gz".format(
        duration, population_size, initial_infection_rate, base_infection_rate, recovery_rate, date_time)


#################################
# Run simulation
#################################
def run_simulation(initial_infection_rate, base_infection_rate, recovery_rate, population_size, duration):
    df = []
    summary = []
    # Seed population at index 0
    df.append(seed_simulation(initial_infection_rate, population_size))
    initially_infected = df[0].count(infected)
    summary.append([0, population_size - initially_infected, initially_infected, 0])
    # Initialise index
    i = 1
    # Loop start running
    while i <= duration:
        # Calculate current stats
        current_infections = df[i - 1].count(infected)
        current_recovered = df[i - 1].count(recovered)
        current_healthy = population_size - current_infections - current_recovered

        # Stop simulation when converged
        if current_infections == 0:
            print("Stopping simulation as current infected = 0")
            break
        # Store summary of previous step
        summary.append([i - 1, current_healthy, current_infections, current_recovered])
        current_infection_rate = compute_current_infection_rate(current_infections, base_infection_rate,
                                                                population_size)

        # Compute new state
        current_state = []
        for case in df[i - 1]:
            current_state.append(compute_transition(current_infection_rate, case, recovery_rate))
        # Store new state
        df.append(current_state)

        # Calculate stats
        prev_perc_completion = round((i - 1) / duration * 100, -1)
        perc_completion = round(i / duration * 100, -1)
        if perc_completion != prev_perc_completion:
            print("Simulation complete {} % - {}".format(perc_completion, datetime.now().strftime("%H:%M:%S")))
        i += 1

    fname = compute_file_name(duration, population_size, initial_infection_rate, base_infection_rate, recovery_rate)
    np.savetxt("./data/data_{}".format(fname), df, delimiter=",")
    np.savetxt("./summary/summary_{}".format(fname), summary, delimiter=",",
               header="time,healthy,infected,recovered")
    return True
