import datetime

import dash_core_components as dcc
import dash_html_components as html
import numpy as np
import pandas as pd
import plotly.graph_objects as go

path = "./summary/"


def load_data(fname):
    df = pd.read_csv(path + fname, dtype={0: np.int64, 1: np.int64, 2: np.int64, 3: np.int64})
    return df


def parse_parameter(fname):
    iterations = fname.split("_")[2]
    size = fname.split("_")[4]
    initialInfectionRate = fname.split("_")[6]
    baseInfectionRate = fname.split("_")[8]
    recovery_rate = fname.split("_")[10]
    date = fname.split("_")[11] + " " + fname.split("_")[12]
    if date[:-3] == "csv":
        date = date[:-4]
    else:
        date = date[:-4]

    date = datetime.datetime.strptime(date, '%Y%m%d %H%M')
    return {'iterations': iterations, 'population': size, 'initialInfectionRate': initialInfectionRate,
            'baseInfectionRate': baseInfectionRate, 'recoveryRate': recovery_rate,
            'date': date.strftime('%d.%m.%y %H:%M')}


def build_label(meta_data):
    return "Initial infection {}, Base infection {}, Recovery {}, Population {}".format(
        meta_data['initialInfectionRate'], meta_data['baseInfectionRate'], meta_data['recoveryRate'],
        meta_data['population'])


def build_graph(fname):
    meta_data = parse_parameter(fname)
    data = load_data(fname)
    data.rename(columns={0: "healthy", 1: "infected", 2: "recovered"})

    # Add traces
    trace_healthy = go.Scatter(x=data.index,
                               y=data.get('healthy'),
                               name='healthy',
                               line=dict(color="LightGreen"))
    trace_infected = go.Scatter(x=data.index,
                                y=data.get('infected'),
                                name='infected',
                                line=dict(color="Red"))
    trace_recovered = go.Scatter(x=data.index, y=data.get('recovered'),
                                 name='recovered',
                                 line=dict(color="Green"))
    data = [trace_healthy, trace_infected, trace_recovered]
    layout = go.Layout(
        title=build_label(meta_data),
        yaxis=dict(
            title='Cases'
        )
    )
    fig = go.Figure(data=data, layout=layout)

    return dcc.Graph(figure=fig)


def get_figures(files):
    if files is None:
        return html.Div(children='no Scenario selected', className='col s12 m6 l4')
    else:
        figs = []
        for file in files:
            figs.append(html.Div(id=file, children=build_graph(file), className='col s12 m6 l6'))
        return figs
